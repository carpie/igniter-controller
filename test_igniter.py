from unittest import mock, TestCase, skip

import machine
import network
from umqtt.simple import MQTTClient

import config
from igniter import (
    ConnectingToNetworkState,
    ConnectingToMqttState,
    DisarmedState,
    ArmedState,
    Igniter,
    InitState,
    MqttReconnectDelayState
)

MODULE = 'igniter'

class IgniterTestCase(TestCase):

    def setUp(self):
        MQTTClient().reset_mock()
        MQTTClient().connect.reset_mock(return_value=True, side_effect=True)
        MQTTClient().wait_msg.reset_mock(return_value=True, side_effect=True)
        MQTTClient().ping.reset_mock(return_value=True, side_effect=True)

    def fake_mqtt_message(self, ig, topic, msg):
        ig.on_mqtt_message(bytes(topic, 'utf-8'), bytes(msg, 'utf-8'))

    def get_to_armed_state(self, ig):
        # Get to armed state
        ig.loop()
        ig.loop()
        ig.loop()
        ig.loop()
        ig.loop()
        self.fake_mqtt_message(ig, config.MQTT_SUB_ARM_TOPIC, '1')
        ig.loop()
        self.assertIsInstance(ig._state, ArmedState)

    @mock.patch('easynet.wlan')
    def test_igniter_happy_path(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.side_effect = [False, True, True]
        ig = Igniter()
        ig.loop()

        # Should have set the io_pins and switch to ConnectingToNetworkState
        machine.Pin.assert_has_calls([
            mock.call(5, machine.Pin.OUT, value=1),
            mock.call(4, machine.Pin.OUT, value=0),
            mock.call(0, machine.Pin.OUT, value=0),
            mock.call(2, machine.Pin.OUT, value=0),
            mock.call(14, machine.Pin.OUT, value=0)
        ])
        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()

        # Should have connected to network and transistioned to mqtt state
        self.assertIsInstance(ig._state, ConnectingToMqttState)
        ig.loop()
        ig.loop()
        ig.loop()

        # Should have connected to mqtt, published status, and transitioned to Disarmed
        ig.mc.publish.assert_has_calls([
            mock.call(config.MQTT_PUB_CONNECTED_TOPIC, b'true', retain=True),
            mock.call(config.MQTT_PUB_ARMED_TOPIC, b'false', retain=True),
            mock.call(config.MQTT_PUB_STATUS_TOPIC, b'Igniter v2.0.0', retain=True)
        ])
        self.assertIsInstance(ig._state, DisarmedState)

        # Sending fire at this point does nothing
        ig.mc.publish.reset_mock()
        self.fake_mqtt_message(ig, config.MQTT_SUB_FIRE_TOPIC, '0')
        ig.mc.publish.assert_not_called()

        # Sending arm command should move to arm state
        self.fake_mqtt_message(ig, config.MQTT_SUB_ARM_TOPIC, '1')
        ig.loop()
        ig.mc.publish.assert_called_with(config.MQTT_PUB_ARMED_TOPIC, b'true', retain=True)
        self.assertIsInstance(ig._state, ArmedState)

        # Should be able to fire all the relays
        for i in range(16):
            # Sending fire at this point fires the correct relay and publishes the fire status
            ig.mc.publish.reset_mock()
            self.fake_mqtt_message(ig, config.MQTT_SUB_FIRE_TOPIC, f'{i}')
            self.assertEqual(ig.relay_outputs, (1 << i))
            ig.mc.publish.assert_called_with(f'{config.MQTT_PUB_RELAY_TOPIC}{i}', b'on', retain=True)
            # Since the timer is mocked, we need to manually deactivate the relay
            ig.deactivate_relay(i)
            self.assertEqual(ig.relay_outputs, 0x0000)
            ig.loop()

        # Sending disarm command should move back to disarm state
        self.fake_mqtt_message(ig, config.MQTT_SUB_ARM_TOPIC, '0')
        ig.loop()
        ig.mc.publish.assert_called_with(config.MQTT_PUB_ARMED_TOPIC, b'false', retain=True)
        self.assertIsInstance(ig._state, DisarmedState)

    @mock.patch('easynet.wlan')
    def test_stays_in_connecting_to_network_state_on_network_error(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = False
        ig = Igniter()
        ig.loop()
        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()
        ig.loop()
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, ConnectingToNetworkState)

    @mock.patch('easynet.wlan')
    def test_starts_over_on_lost_connection(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = True
        # Force an error waiting for mqtt
        MQTTClient().wait_msg.side_effect=OSError('somebody cut the line')
        ig = Igniter()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToMqttState)
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, DisarmedState)
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, InitState)

    @mock.patch('easynet.wlan')
    def test_starts_over_on_ping_error(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = True
        # Force an error waiting for mqtt
        MQTTClient().ping.side_effect=OSError('no pong')
        ig = Igniter()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToMqttState)
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, DisarmedState)
        ig.loop()
        ig.loop()
        ig.mqtt_ping(0)
        self.assertIsInstance(ig._state, InitState)

    @mock.patch('easynet.wlan')
    def test_starts_if_network_dies_during_mqtt_connect(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.side_effect = [True, False]
        # Force an error waiting for mqtt
        MQTTClient().ping.side_effect=OSError('no pong')
        ig = Igniter()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToMqttState)
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, InitState)


    @mock.patch('easynet.wlan')
    def test_mqtt_retries_connection(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = True
        # Force an error waiting for connect
        MQTTClient().connect.side_effect=OSError('not going to connect yo')
        ig = Igniter()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToNetworkState)
        ig.loop()
        ig.loop()

        self.assertIsInstance(ig._state, ConnectingToMqttState)
        ig.loop()
        ig.loop()
        self.assertIsInstance(ig._state, MqttReconnectDelayState)
        ig.loop()
        self.assertIsInstance(ig._state, ConnectingToMqttState)

    @mock.patch('easynet.wlan')
    def test_handles_malformed_mqtt_messages(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = True

        ig = Igniter()
        self.get_to_armed_state(ig)

        # Send a malformed fire message
        self.fake_mqtt_message(ig, config.MQTT_SUB_FIRE_TOPIC, 'all of em')
        # Send a malformed disarm message
        self.fake_mqtt_message(ig, config.MQTT_SUB_ARM_TOPIC, 'arm it!')

    @mock.patch('easynet.wlan')
    def test_handles_out_of_range_fire_indicies(self, wlan):
        wlan.status.return_value = True
        wlan.isconnected.return_value = True

        ig = Igniter()
        self.get_to_armed_state(ig)

        # Send an out-of-bounds fire message
        self.fake_mqtt_message(ig, config.MQTT_SUB_FIRE_TOPIC, '-1')
        self.fake_mqtt_message(ig, config.MQTT_SUB_FIRE_TOPIC, '16')
