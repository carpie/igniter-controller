import machine
import struct
import utime as time
from umqtt.simple import MQTTClient

import config
from easynet import enable_access_point, connect_to_network, is_network_connected
from shift import ShiftRegister

class State():
    @property
    def context(self):
        return self._context

    @context.setter
    def context(self, context):
        self._context = context

    def tick(self): #pragma no cover
        pass

    def on_entry(self):
        pass

    def on_exit(self):
        pass

    # Possible events
    def network_connect_status(self, err): #pragma no cover
        pass

    def mqtt_connect_status(self, err): #pragma no cover
        pass

    def arm_system_request(self): #pragma no cover
        pass

    def disarm_system_request(self): #pragma no cover
        pass

    def fire_relay_request(self, index):
        pass

    def lost_connection(self):
        # If we lose connection at any point, start over
        self.context.cancel_ping_timer()
        self.context.setState(InitState())


class InitState(State):
    def on_entry(self):
        self.context.init_io_pins()
        self.context.disable_access_point()
        self.context.setState(ConnectingToNetworkState())


class ConnectingToNetworkState(State):
    def tick(self):
        self.context.connect_to_network()

    def network_connect_status(self, err):
        if err:
            print(f'Error connecting to network {err}')
            return;
        print('  -- network connected')
        self.context.setState(ConnectingToMqttState())


class ConnectingToMqttState(State):
    def tick(self):
        self.context.connect_to_mqtt(self.context.mqtt_connect_status)

    def mqtt_connect_status(self, err):
        if err:
            print(f'  -- mqtt connection error {err}')
            if err == 'Network not connected':
                self.context.setState(InitState())
            else:
                self.context.setState(MqttReconnectDelayState())
            return
        print('  -- mqtt connected')
        self.context.start_ping_timer()
        self.context.setState(DisarmedState())


class MqttReconnectDelayState(State):
    def on_entry(self):
        time.sleep(5)
        self.context.setState(ConnectingToMqttState())


class DisarmedState(State):
    def on_entry(self):
        self.context.mc.publish(config.MQTT_PUB_ARMED_TOPIC, b'false', retain=True)

    def tick(self):
        self.context.mqtt_wait()

    def arm_system_request(self):
        self.context.setState(ArmedState())


class ArmedState(State):
    def on_entry(self):
        self.context.set_safe_led(False)
        self.context.mc.publish(config.MQTT_PUB_ARMED_TOPIC, b'true', retain=True)

    def on_exit(self):
        self.context.set_safe_led(True)

    def tick(self):
        self.context.mqtt_wait()

    def disarm_system_request(self):
        self.context.setState(DisarmedState())

    def fire_relay_request(self, index):
        self.context.fire_relay(index)


class Igniter:
    def __init__(self):
        self._ping_timer = None
        self._previous_state = None
        self.setState(InitState())

    def setState(self, state):
        self._state = state
        self._state.context = self

    def _handle_transition(self):
        if self._previous_state != self._state:
            cached_state = self._state
            print(f'--  {type(self._previous_state).__name__} ==> {type(self._state).__name__}')
            if (self._previous_state):
                self._previous_state.on_exit()
            self._state.on_entry()
            self._previous_state = cached_state
            return True
        return False

    def loop(self):
        if not self._handle_transition():
            self._state.tick()

    def run(self): #pragma no cover
        while True:
            self.loop()

    # Handlers
    def on_mqtt_message(self, topic, msg):
        decoded_topic = topic.decode()
        if decoded_topic == config.MQTT_SUB_FIRE_TOPIC:
            try:
                index = int(msg)
                self.fire_relay_request(index)
            except (ValueError):
                pass
        elif decoded_topic == config.MQTT_SUB_ARM_TOPIC:
            try:
                do_arm = int(msg)
                if do_arm:
                    self.arm_system_request()
                else:
                    self.disarm_system_request()
            except (ValueError):
                pass

    # Events
    def arm_system_request(self):
        self._state.arm_system_request()

    def disarm_system_request(self):
        self._state.disarm_system_request()

    def fire_relay_request(self, index):
        self._state.fire_relay_request(index)

    def mqtt_connect_status(self, err):
        self._state.mqtt_connect_status(err)

    def network_connect_status(self, err):
        self._state.network_connect_status(err)

    def lost_connection(self):
        self._state.lost_connection()


    # Actions
    def disable_access_point(self):
        enable_access_point(False)

    def start_ping_timer(self):
        self._ping_timer = machine.Timer(-1)
        self._ping_timer.init(mode=machine.Timer.PERIODIC, period=5000, callback=self.mqtt_ping)

    def cancel_ping_timer(self):
        if self._ping_timer:
            self._ping_timer.deinit()
        self._ping_timer = None

    def write_relay_outputs(self):
        self.sr.load(struct.pack('H', self.relay_outputs))

    def init_io_pins(self):
        self.oe = machine.Pin(5, machine.Pin.OUT, value=1) # D1
        self.latch = machine.Pin(4, machine.Pin.OUT, value=0) # D2
        self.clk = machine.Pin(0, machine.Pin.OUT, value=0) # D3
        self.data = machine.Pin(2, machine.Pin.OUT, value=0) # D4
        self.safe_led = machine.Pin(14, machine.Pin.OUT, value=0) # D5
        # Turn off all output pins on the shift register
        self.relay_outputs = 0x0000
        self.sr = ShiftRegister(self.data, self.clk, self.oe, self.latch, is_active_low=True)
        self.write_relay_outputs()
        self.sr.enable_outputs()

    def connect_to_network(self):
        connect_to_network(self.network_connect_status, 3)

    def connect_to_mqtt(self, callback):
        print(' -- Spinning up MQTT client...')
        self.mc = MQTTClient(config.MQTT_CLIENT_ID, config.MQTT_SERVER, keepalive=10)
        self.mc.set_callback(self.on_mqtt_message)
        self.mc.set_last_will(config.MQTT_PUB_CONNECTED_TOPIC, b'false', retain=True)
        try:
            if not is_network_connected():
                return callback('Network not connected')
            self.mc.connect()
            self.mc.publish(config.MQTT_PUB_CONNECTED_TOPIC, b'true', retain=True)
            self.mc.publish(config.MQTT_PUB_ARMED_TOPIC, b'false', retain=True)
            self.mc.publish(config.MQTT_PUB_STATUS_TOPIC, b'Igniter v2.0.0', retain=True)
            self.mc.subscribe(config.MQTT_SUB_ARM_TOPIC)
            self.mc.subscribe(config.MQTT_SUB_FIRE_TOPIC)
            # Set a timer to keep us alive
        except OSError as e:
            return callback(e)
        return callback(None)

    def mqtt_ping(self, _):
        try:
            self.mc.ping()
        except OSError as e:
            print(f'{e}')
            self.lost_connection()

    def mqtt_wait(self):
        try:
            self.mc.wait_msg()
        except OSError as e:
            print(f'{e}')
            self.lost_connection()

    def set_output_bit(self, index, value):
        mask = 0x0001
        mask <<= index
        if value:
            self.relay_outputs |= mask
        else:
            self.relay_outputs &= (~mask & 0xFFFF)

    def fire_relay(self, index):
        if index < 0 or index > 15:
            return
        print(' -- firing relay', index)
        t = machine.Timer(-1)
        t.init(mode=machine.Timer.ONE_SHOT, period=2000, callback=lambda _: self.deactivate_relay(index))
        self.set_output_bit(index, 1)
        self.write_relay_outputs()
        self.mc.publish(f'{config.MQTT_PUB_RELAY_TOPIC}{index}', b'on', retain=True)

    def deactivate_relay(self, index):
        print(' -- deactivating relay', index)
        self.set_output_bit(index, 0)
        self.write_relay_outputs()
        self.mc.publish(f'{config.MQTT_PUB_RELAY_TOPIC}{index}', b'off', retain=True)

    def set_safe_led(self, active):
        if active:
            self.safe_led(0)
        else:
            self.safe_led(1)


def main(): #pragma no cover
    ig = Igniter()
    ig.run()
