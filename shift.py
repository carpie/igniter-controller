class ShiftRegister:
    def __init__(self, data, clk, enable, latch, is_active_low=False):
        self.data = data
        self.clk = clk
        self.enable = enable
        self.latch_pin = latch
        self.is_active_low = is_active_low

    def enable_outputs(self):
        self.enable(0)

    def disable_outputs(self):
        self.enable(1)

    def _shift(self, val):
        self.data(val & 0x1)
        self.clk(1)
        self.clk(0)

    def latch(self):
        self.latch_pin(1)
        self.latch_pin(0)

    def load_byte(self, val):
        data = (~val) & 0xFF if self.is_active_low else (val & 0xFF)
        for i in range(7, -1, -1):
            self._shift(data >> i)

    def load(self, buf, auto_latch=True):
        for i in range(len(buf) -1, -1, -1):
            self.load_byte(buf[i])
        if auto_latch:
            self.latch()
