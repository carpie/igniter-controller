# Igniter Relay Driver

This repo contains firmware for an ESP8266 driving a set of up to 16 active low
relays.  The relays are driven from a shift register which is bit-bang driven
from the ESP8266.  Relay fire commands are sent via MQTT.

The pin connections from the ESP8266 are as follows (GPIO-numbers):
- GPIO5 - output enable for shift register
- GPIO4 - latch for shift register's internal register to output pins
- GPIO0 - clock signal for shifting data into shift register
- GPIO2 - serial data output feeding shift register
- GPIO14 - "safe" LED driver (turns off in Armed mode)

The firmware connects to wifi and then to an MQTT server you specify in
`config.py`.  From there you can send MQTT commands to fire the various relays.
To arm the system, send `/ignite/arm/1`. To disarm the system, send
`/ignite/arm/0`.  To fire relays, send `/ignite/relay/0` where `0` is the index
of the relay to fire.  The relay will enable for 2 seconds and then
automatically turn off.

The firmware reports general status on `/ignite/status`, wifi connection status
(true/false) on `/ignite/connected`, armed status (true/false) on
`/ignite/armed`, and relay status (on/off) on `/ignite/relay/0` where `0` is
the index of the relay being reported.

## Installing firmware on ESP8266
- Get support tools in a virtual environment
```
python3 -mvenv .ve
. .ve/bin/activate
pip install -r requirements.txt
```
- Install micropython
```
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20180511-v1.9.4.bin
```
- Copy source to microcontroller
```
export AMPY_PORT=/dev/ttyUSB0
ampy put config_edit.py
ampy put config.py
ampy put easynet_config.py
ampy put easynet.py
ampy put igniter.py
ampy put shift.py
```
- Connect to device via terminal
```
screen /dev/ttyUSB0 115200
```
- Edit network config on device (in REPL):
```
from config_edit import edit_config
edit_config('easynet_config')
```
- Put auto-startup file on device:
```
ampy put main.py
```
- Reboot device

## Running unit tests

Tests can be run and a coverage report generated in a single step via:

```
coverage run -m unittest && coverage report -m
```

If you just want to run tests:

```
python -m unittest
```
