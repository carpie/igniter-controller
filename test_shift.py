from unittest import mock, TestCase, skip

from shift import ShiftRegister

data = mock.MagicMock()
clk = mock.MagicMock()
enable = mock.MagicMock()
latch = mock.MagicMock()

class ShiftRegisterTestCase(TestCase):

    def setUp(self):
        data.reset_mock()
        clk.reset_mock()
        enable.reset_mock()
        latch.reset_mock()
        
    def test_load_active_high_a5a5(self):
        sr = ShiftRegister(data, clk, enable, latch)
        sr.load(b'\xA5\xA5')
        data.assert_has_calls([
            mock.call(1),
            mock.call(0),
            mock.call(1),
            mock.call(0),

            mock.call(0),
            mock.call(1),
            mock.call(0),
            mock.call(1),

            mock.call(1),
            mock.call(0),
            mock.call(1),
            mock.call(0),

            mock.call(0),
            mock.call(1),
            mock.call(0),
            mock.call(1),
        ])
        clk.assert_has_calls([mock.call(1), mock.call(0)] * 16)
        self.assertEqual(clk.call_count, 32)
        # Auto latch should have automatically latched
        latch.assert_has_calls([mock.call(1), mock.call(0)])
        self.assertEqual(latch.call_count, 2)

    def test_load_active_low_a5a5(self):
        sr = ShiftRegister(data, clk, enable, latch, is_active_low=True)
        sr.load(b'\xA5\xA5', auto_latch=False)
        data.assert_has_calls([
            mock.call(0),
            mock.call(1),
            mock.call(0),
            mock.call(1),

            mock.call(1),
            mock.call(0),
            mock.call(1),
            mock.call(0),

            mock.call(0),
            mock.call(1),
            mock.call(0),
            mock.call(1),

            mock.call(1),
            mock.call(0),
            mock.call(1),
            mock.call(0),
        ])
        clk.assert_has_calls([mock.call(1), mock.call(0)] * 16)
        self.assertEqual(clk.call_count, 32)
        # Auto latch should not have automatically latched
        self.assertEqual(latch.call_count, 0)

    def test_load_active_high_ff00(self):
        sr = ShiftRegister(data, clk, enable, latch)
        sr.load(b'\xff\x00')
        data.assert_has_calls([
            mock.call(0),
            mock.call(0),
            mock.call(0),
            mock.call(0),

            mock.call(0),
            mock.call(0),
            mock.call(0),
            mock.call(0),

            mock.call(1),
            mock.call(1),
            mock.call(1),
            mock.call(1),

            mock.call(1),
            mock.call(1),
            mock.call(1),
            mock.call(1),
        ])
        clk.assert_has_calls([mock.call(1), mock.call(0)] * 16)
        self.assertEqual(clk.call_count, 32)
        # Auto latch should have automatically latched
        latch.assert_has_calls([mock.call(1), mock.call(0)])
        self.assertEqual(latch.call_count, 2)

    def test_load_active_low_ff00(self):
        sr = ShiftRegister(data, clk, enable, latch, is_active_low=True)
        sr.load(b'\xff\x00', auto_latch=False)
        data.assert_has_calls([
            mock.call(1),
            mock.call(1),
            mock.call(1),
            mock.call(1),

            mock.call(1),
            mock.call(1),
            mock.call(1),
            mock.call(1),

            mock.call(0),
            mock.call(0),
            mock.call(0),
            mock.call(0),

            mock.call(0),
            mock.call(0),
            mock.call(0),
            mock.call(0),
        ])
        clk.assert_has_calls([mock.call(1), mock.call(0)] * 16)
        self.assertEqual(clk.call_count, 32)
        # Auto latch should not have automatically latched
        self.assertEqual(latch.call_count, 0)

    def test_can_enable_disable_outputs(self):
        sr = ShiftRegister(data, clk, enable, latch)
        sr.enable_outputs();
        enable.assert_called_once_with(0)
        enable.reset_mock()
        sr.disable_outputs();
        enable.assert_called_once_with(1)
