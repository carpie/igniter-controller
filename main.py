import machine
import sys
import uos

import igniter

try:
    igniter.main()
except KeyboardInterrupt:
    pass
except Exception as e:
    print(e)
    sys.print_exception(e)
    machine.reset()
