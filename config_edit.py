import sys

def edit_config(name):
    config = __import__(name)    
    keys = [x for x in dir(config) if not x.startswith('__')]

    for key in keys:
        orig_value = getattr(config, key)
        value = input('{}: [{}]: '.format(key, orig_value))
        if len(value) > 0:
            if isinstance(orig_value, bool):
                setattr(config, key, value == 'True')
            elif isinstance(orig_value, int):
                setattr(config, key, int(value))
            elif isinstance(orig_value, float):
                setattr(config, key, float(value))
            else:
                setattr(config, key, value)

    fp = open('{}.py'.format(name), 'w')

    for key in keys:
        value = getattr(config, key)
        if isinstance(value, str):
            value = value.replace("'", r"\'")
            fp.write("{} = '{}'\n".format(key, value))
        else:
            fp.write("{} = {}\n".format(key, value))
    fp.close()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: {} config_module'.format(sys.argv[0]));
        sys.exit(1)
    edit_config(sys.argv[1])
